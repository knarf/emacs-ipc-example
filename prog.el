;;; prog.el --- Example of IPC using TCP sockets

;; Author: Aurélien Aptel <aurelien.aptel@gmail.com>

(defun prog-length (pid str)
  "Connect to prog PID, send STR, read and return length."
  (let (buf proc port res)
    (setq port (with-temp-buffer
                 (insert-file-contents
                  (format "/tmp/prog-port-%d" pid))
                 (buffer-string)))

    (with-temp-buffer
      (setq proc (open-network-stream "prog" (current-buffer) "localhost" port))
      (process-send-string proc (concat str "\n"))
      (accept-process-output proc 1 nil t)
      (prog1 (read (buffer-string))
        (delete-process proc)))))

(provide 'prog)

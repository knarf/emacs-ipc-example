#!/usr/bin/perl
use strict;
use warnings;
use IO::Socket::INET;

# creating a listening socket
my $socket = new IO::Socket::INET (
    LocalHost => '0.0.0.0',
    Proto => 'tcp',
    Listen => 5,
    Reuse => 1

    # by omiting the port, system chooses a random available one
    # LocalPort => 7777,
);

die "cannot create socket $!\n" if !$socket;

# don't buffer read/write
$socket->autoflush(1);

my $PID = $$;
my $port = $socket->sockport();
print "PID $PID listening on port $port\n";

# write port number to a file for emacs to read
open my $f, '>', "/tmp/prog-port-$PID" or die "can't write port number file";
print $f "$port";
close $f;

while(1) {
    # waiting for a new client connection (blocking call)
    my $client = $socket->accept();

    # get information about the newly connected client
    my $address = $client->peerhost();
    print "connection from $address\n";

    # read line from client
    my $data = <$client>;
    print "received $data";

    # write response data to the connected client
    # simply return line length (without newline)
    $data = length($data)-1;
    print $client "$data\n";

    # nothing left to do? close connection
    close $client;
}

$socket->close();
